
import UIKit


class Board {
    let boardSize: Int
    let dice: [Die]
    let players: [Player]
    let actionTiles: [ActionTile]
    
    init (boardSize: Int, dice: [Die], players: [Player], actionTiles: [ActionTile]) {
        self.boardSize = boardSize
        self.dice = dice
        self.players = players
        self.actionTiles = actionTiles
    }
    
    func rollDice() -> Int {
        var totalMoves: Int = 0
        for die in dice {
             totalMoves += die.roll()
        }
        return totalMoves
    }
    
    func checkWinner(player: Player) -> Bool {
            if player.playerPosition > boardSize {
                player.playerPosition = boardSize - player.playerPosition % boardSize
                
            } else if player.playerPosition == boardSize {
                return true
            }
        return false
    }
    
    func playGame() {
        var hasWinner: Bool = false
        while !hasWinner {
            for player in players {
                let totalMoves: Int = rollDice()
                player.movePlayer(totalMoves)
                if (!checkWinner(player)) {
                    print("\(player.name) rolled \(totalMoves) and he is at \(player.playerPosition).")
                } else {
                    print("\(player.name) rolled \(totalMoves) and he is at \(player.playerPosition) and he WON!")
                    hasWinner = true
                    break
                }
                for actionTile in actionTiles {
                    player.playerPosition = actionTile.checkAction(player.name, playerPosition: player.playerPosition)
                }
               
                
            }
            
        }
    }
}

class Die {
    private let numberOfSides: Int
    init (numberOfSides: Int = 6) {
        self.numberOfSides = numberOfSides
    }
    
    func roll() -> Int {
        return Int(arc4random() % UInt32(self.numberOfSides)) + 1
    }
}

class Player {
    var name: String
    var playerPosition: Int = 0
    
    init (name: String) {
        self.name = name
    }
    
    func movePlayer(totalMoves: Int) {
        playerPosition += totalMoves
    }
}

class ActionTile {
    var action: String
    var beginPosition: Int
    var endPosition: Int
    
    init(action:String, beginPosition: Int, endPosition: Int) {
        self.action = action
        self.beginPosition = beginPosition
        self.endPosition = endPosition
    }
    
    func checkAction(playerName:String, playerPosition: Int) -> Int {
        if (playerPosition == beginPosition) {
            print("There is a \(action) and \(playerName) move to \(endPosition)")
            return endPosition
        }
        return playerPosition
    }
}

// Initial variables
let actionTiles: [ActionTile] = [ActionTile(action: "Ladder", beginPosition: 05, endPosition: 20),
                                  ActionTile.init(action: "Ladder", beginPosition: 13, endPosition: 32),
                                  ActionTile.init(action: "Ladder", beginPosition: 40, endPosition: 62),
                                  ActionTile.init(action: "Ladder", beginPosition: 82, endPosition: 93),
                                  ActionTile.init(action: "Ladder", beginPosition: 02, endPosition: 18),
                                  ActionTile.init(action: "Ladder", beginPosition: 03, endPosition: 15),
                                  ActionTile.init(action: "Ladder", beginPosition: 35, endPosition: 53),
                                  ActionTile.init(action: "Snake", beginPosition: 73, endPosition: 33),
                                  ActionTile.init(action: "Snake", beginPosition: 99, endPosition: 01),
                                  ActionTile.init(action: "Snake", beginPosition: 80, endPosition: 10),
                                  ActionTile.init(action: "Snake", beginPosition: 56, endPosition: 43),
                                  ActionTile.init(action: "Snake", beginPosition: 32, endPosition: 08)]

let players: [Player] = [Player(name: "Bob"), Player(name: "Apple")]
let dice: [Die] = [Die(numberOfSides: 6), Die(numberOfSides: 6)]
let board: Board = Board(boardSize: 100, dice: dice, players: players, actionTiles: actionTiles)

board.playGame()